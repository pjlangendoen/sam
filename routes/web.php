<?php
require 'web/admin.php';
require 'web/site.php';
require 'web/forms.php';
Auth::routes();

Route::get('/home', 'HomeController@index');
