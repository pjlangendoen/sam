<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Form', 'middleware' => 'auth'], function () {
    Route::get('/household-new', [
        'as' => 'forms.household-new',
        'uses' => 'FormController@householdnew'
    ]);

});