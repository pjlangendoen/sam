<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('/dashboard', [
        'as' => 'admin.dashboard',
        'uses' => 'HomeController@index'
    ]);

    Route::get('/household', [
        'as' => 'admin.household',
        'uses' => 'HomeController@household'
    ]);

    Route::get('/media', [
        'as' => 'admin.media',
        'uses' => 'HomeController@media'
    ]);

    Route::get('/user', [
        'as' => 'admin.user',
        'uses' => 'HomeController@user'
    ]);
});