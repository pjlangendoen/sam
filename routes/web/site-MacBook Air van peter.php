<?php

Route::group(['namespace' => 'Site'], function () {
    Route::get('/', [
        'as' => 'site.dashboard',
        'uses' => 'HomeController@index'
    ]);

    Route::get('/user', 'AuthController@user');
});

Route::group(['namespace' => 'Auth'], function () {
    Route::get('/logout', [
        'as' => 'Auth.logout',
        'uses' => 'LoginController@logout'
    ]);

});