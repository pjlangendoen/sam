<?php

Route::group(['namespace' => 'Site'], function () {

    Route::get('/', function () {
        if (Auth::check()) {
            return redirect()->route('admin.dashboard');
        }
        return view('Auth.login');
    });

});



Route::group(['namespace' => 'Auth'], function () {
    Route::get('/logout', [
        'as' => 'Auth.logout',
        'uses' => 'LoginController@logout'
    ]);
});

