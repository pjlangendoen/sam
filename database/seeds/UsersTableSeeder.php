<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'peter',
            'email' => 'peter@propic.nl',
            'password' => bcrypt('peter'),
            'lastname' => 'Tester',
        ]);
        \App\User::create([
            'name' => 'jeroen',
            'email' => 'jeroen@propic.nl',
            'password' => bcrypt('jeroen'),
            'lastname' => 'Tester',
        ]);
    }
}
