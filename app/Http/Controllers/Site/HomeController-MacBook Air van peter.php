<?php

namespace App\Http\Controllers\Site;

class HomeController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        return view('site.dashboard');
    }
}