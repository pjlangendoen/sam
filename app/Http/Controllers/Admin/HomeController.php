<?php

namespace App\Http\Controllers\Admin;

class HomeController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        return view('admin.dashboard');
    }
    public function media()
    {
        return view('admin.media');
    }
    public function user()
    {
        return view('admin.user');
    }

    public function household()
    {
        return view('admin.household');
    }
    
}